package app

import "github.com/fuxiaohei/hxgo/database"

// database vars
var (
	Db *database.Db
)

// init database
func initDb() {
	if Cfg.Bool("DbEnable") {
		Db = database.NewDb(Cfg.String("DbDriver"), Cfg.String("DbDsn"), Cfg.Int("DbConnMax"))
		Db.IsPanic = true
		Log.Info("[" + Name + "] load " + Cfg.String("DbDriver"))
	}
}

// enable db if init none, return db is created or not
// you can change config dynamic and re-connect db
func EnableDb() bool {
	initDb()
	return Db == nil
}

